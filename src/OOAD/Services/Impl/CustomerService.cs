﻿using OOAD.Services.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OOAD.Models;
using OOAD.Repositories.Api;

namespace OOAD.Services.Impl
{
    public class CustomerService : ICustomerService
    {
        private IStripeCustomerService stripeCustomers;
        private ICustomerRepository customers;

        public CustomerService(IStripeCustomerService stripeCustomers, ICustomerRepository customers)
        {
            this.stripeCustomers = stripeCustomers;
            this.customers = customers;
        }

        public Customer Create(Customer customer)
        {
            // the following lines of code show the genereal flow, not all issues are handled
            var stripeCustomerId = stripeCustomers.Create(customer);
            customer.StripeCustomerId = stripeCustomerId;
            customers.Add(customer);

            throw new NotImplementedException();
        }

        public Customer Get(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
