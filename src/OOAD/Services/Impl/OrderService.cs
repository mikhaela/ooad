﻿using OOAD.Services.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OOAD.Models.Entities;
using OOAD.Repositories.Api;

namespace OOAD.Services.Impl
{
    public class OrderService : IOrderService
    {
        private IOrderRepository orders;

        public OrderService(IOrderRepository orders)
        {
            this.orders = orders;
        }

        public void Create(Order order)
        {
            orders.Add(order);
            throw new NotImplementedException();
        }
    }
}
