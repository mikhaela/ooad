﻿using OOAD.Services.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OOAD.Models;
using OOAD.Repositories.Api;
using OOAD.Models.Dto;

namespace OOAD.Services.Impl
{
    public class OrderProcessingService : IOrderProcessingService
    {

        private readonly IChargeService chargeService;
        private readonly ICustomerService customerService;
        private readonly IOrderService orderService;

        public OrderProcessingService(IChargeService chargeService, IOrderService orderService, ICustomerService customerService)
        {
            this.chargeService = chargeService;
            this.orderService = orderService;
            this.customerService = customerService;
        }

        public void Process(OrderDto orderDto)
        {
            // the following lines of code show the genereal flow, not all issues are handled
            var customer = customerService.Get(orderDto.Customer.Id);
            if (customer == null)
            {
                customer = customerService.Create(customer);
            }

            var order = orderDto.Order;
            order.CustomerId = customer.Id;

            orderService.Create(order);

            var charge = new Charge()
            {
                Token = orderDto.StripeChargeToken,
                CustomerId = customer.StripeCustomerId,
                Money = order.Price
            };

            chargeService.CreateCharge(charge);

            // update the status of the order depending  on the result of the charge processing
            throw new NotImplementedException();
        }
    }
}
