﻿using OOAD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Services.Api
{
    public interface IOrderProcessingService
    {
        void Process(OrderDto orderDto);
    }
}
