﻿using OOAD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Services.Api
{
    public interface ICustomerService
    {
        Customer Create(Customer customer);
        Customer Get(Guid id);
    }
}
