﻿using OOAD.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Services.Api
{
    public interface IOrderService
    {
        void Create(Order order);
    }
}
