﻿using OOAD.Models.Dto;

namespace OOAD.Services.Api
{
    public interface IChargeService
    {
        bool CreateCharge(Charge charge);
    }
}
