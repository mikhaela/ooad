﻿using OOAD.Models;

namespace OOAD.Services.Api
{
    public interface IStripeCustomerService
    {
        string Create(Customer customer);
    }
}
