﻿using Microsoft.AspNetCore.Mvc;
using OOAD.Models;
using OOAD.Services.Api;
using System;

namespace OOAD.Controllers
{
    public class OrderApiController : Controller
    {

        private IOrderProcessingService orderProcessor;

        public OrderApiController(IOrderProcessingService orderProcessor)
        {
            this.orderProcessor = orderProcessor;
        }

        [HttpGet]
        public IActionResult Get()
        {
            throw new NotImplementedException();
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute]Guid id)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public IActionResult Post([FromBody]OrderDto orderDto)
        {
            orderProcessor.Process(orderDto);

            throw new NotImplementedException();
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromRoute]Guid id, [FromBody]OrderDto orderDto)
        {
            throw new NotImplementedException();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
