﻿using OOAD.Data;
using OOAD.Repositories.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OOAD.Models;

namespace OOAD.Repositories.Impl
{
    public class CustomerRepository : ICustomerRepository
    {
        private ApplicationDbContext db;

        public CustomerRepository(ApplicationDbContext db)
        {
            this.db = db;
        }

        public Customer Add(Customer customer)
        {
            db.Set<Customer>().Add(customer);
            throw new NotImplementedException();
        }

        public bool Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Customer> Get()
        {
            throw new NotImplementedException();
        }

        public Customer Get(Guid id)
        {
            throw new NotImplementedException();
        }

        public void Update(Customer customer)
        {
            throw new NotImplementedException();
        }
    }
}
