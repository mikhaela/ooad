﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OOAD.Models.Entities;
using OOAD.Repositories.Api;
using OOAD.Data;

namespace OOAD.Repositories.Impl
{
    public class OrderRepository : IOrderRepository
    {

        private ApplicationDbContext db;

        public OrderRepository(ApplicationDbContext db)
        {
            this.db = db;
        }

        public Order Add(Order order)
        {
            db.Set<Order>().Add(order);
            throw new NotImplementedException();
        }

        public bool Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Order> Get()
        {
            throw new NotImplementedException();
        }

        public Order Get(Guid id)
        {
            throw new NotImplementedException();
        }

        public void Update(Order order)
        {
            throw new NotImplementedException();
        }
    }
}
