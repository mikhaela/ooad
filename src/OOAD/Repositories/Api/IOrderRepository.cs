﻿using OOAD.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Repositories.Api
{
    // information expert responsible for executing CRUD operations with entities Order, OrderLine, Shipment, Payment 
    public interface IOrderRepository
    {
        Order Add(Order order);
        void Update(Order order);
        bool Delete(Guid id);
        IEnumerable<Order> Get();
        Order Get(Guid id);
    }
}
