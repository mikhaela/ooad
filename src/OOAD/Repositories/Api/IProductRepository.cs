﻿using OOAD.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Repositories.Api
{
    // information expert responsible for executing CRUD operations with entities Product and ProductDetail 
    public interface IProductRepository
    {
        Product Add(Product product);
        void Update(Product product);
        bool Delete(Guid id);
        IEnumerable<Product> Get();
        Product Get(Guid id);
    }
}
