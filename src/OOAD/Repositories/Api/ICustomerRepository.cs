﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OOAD.Models;

namespace OOAD.Repositories.Api
{
    // information expert responsible for executing CRUD operations with entity Customer
    public interface ICustomerRepository
    {
        Customer Add(Customer customer);
        void Update(Customer customer);
        bool Delete(Guid id);
        IEnumerable<Customer> Get();
        Customer Get(Guid id); 
    }
}
