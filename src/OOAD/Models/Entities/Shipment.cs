﻿using OOAD.Models.Vo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Models
{
    public class Shipment
    {
        public Guid Id { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public Address Address { get; set; }
        public ShipmentStatus Status { get; set; }
    }
}
