﻿using OOAD.Models.Vo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Models.Entities
{
    public class Order
    {
        public Guid Id { get; set; }
        public OrderStatus Status { get; set; }
        public DateTime OrderDatetime { get; set; }

        public List<OrderLine> OrderLines { get; set; }

        public Guid CustomerId { get; set; } 
        public Customer Customer { get; set; }

        public Guid PaymentId { get; set; }
        public Payment Payment { get; set; }

        public Guid ShipmentId { get; set; }
        public Shipment Shipment { get; set; }
        public Money Price { get; internal set; }
    }
}
