﻿using OOAD.Models.Vo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Models
{
    public class Customer
    {
        public Guid Id { get; set; }
        public Name Name { get; set; }
        public string Email { get; set; }
        public string StripeCustomerId { get; set; }
    }
}
