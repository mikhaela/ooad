﻿using OOAD.Models.Vo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Models.Entities
{
    public class Product
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Money UnitPrice { get; set; }
        public UnitType Unit { get; set; }

        public List<ProductDetail> ProductDetails { get; set; }
    }
}
