﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Models.Entities
{
    public class OrderLine
    {
        public Guid Id { get; set; }
        public int Quantity { get; set; }

        public Guid ProductId {get; set; }
        public Product Product { get; set; }
    }
}
