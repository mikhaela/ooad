﻿using OOAD.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Models
{
    public class OrderDto
    {
        public Order Order { get; set; }
        public Customer Customer { get; set; }
        public string StripeChargeToken { get; set; }
    }
}
