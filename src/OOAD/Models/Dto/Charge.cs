﻿using OOAD.Models.Vo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Models.Dto
{
    public class Charge
    {
        public Money Money { get; set; }
        public string CustomerId { get; set; }
        public string Token { get; set; }
    }
}
