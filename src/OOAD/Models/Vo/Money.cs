﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Models.Vo
{
    public class Money
    {
        public int AmountInCoins { get; set; }
        public Currency Currency { get; set; }
    }
}
