﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Models.Vo
{
    public enum Currency
    {
        UAH,
        EUR,
        USD
    }
}
