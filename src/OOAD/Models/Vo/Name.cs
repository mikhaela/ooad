﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOAD.Models.Vo
{
    public struct Name
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
